CFLAGS  += -std=c11
LDFLAGS += -lpthread

all: fastrandom fastrandom.1

clean:
	-rm fastrandom
#	-rm fastrandom.1
#   Not everyone has pandoc installed...
	-rm fastrandom.1.gz
	-rm test_result

install: fastrandom fastrandom.1.gz
	install -Dm755 -o0 -g0 fastrandom $(DESTDIR)/usr/bin/fastrandom
	install -Dm644 -o0 -g0 fastrandom.1.gz $(DESTDIR)/usr/share/man/man1/fastrandom.1.gz

fastrandom: fastrandom.c

fastrandom.1: fastrandom.1.md
	pandoc -s -t man -o $@ $<

fastrandom.1.gz: fastrandom.1
	gzip -k -f $<

test: fastrandom
	@/bin/sh ./test.sh
