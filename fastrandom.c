/*
*  Copyright (c) 2022  Andre Kugland
*
*  Permission is hereby granted, free of charge, to any person obtaining a copy
*  of this software and associated documentation files (the "Software"), to deal
*  in the Software without restriction, including without limitation the rights
*  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
*  copies of the Software, and to permit persons to whom the Software is
*  furnished to do so, subject to the following conditions:
*
*  The above copyright notice and this permission notice shall be included in all
*  copies or substantial portions of the Software.
*
*  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
*  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
*  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
*  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
*  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
*  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
*  SOFTWARE.
*/

#include <stdatomic.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <threads.h>
#include <errno.h>
#include <getopt.h>

#define BUFFER_SIZE (128 * 1024)    /* 128 * 1024 * sizeof(uint64_t) = 1 MB */

static bool infinite_stream = true;        /* If false, emit data until remaining_bytes == 0 */
static atomic_ulong remaining_bytes;       /* Number of remaining bytes */
static unsigned number_of_threads = 2;     /* Number of threads */

const char* version = "fastrandom 1.0.8\n";

const char help_text[] =
    "Usage: fastrandom [OPTION]...\n"
    "Quickly generate large amounts of cryptographically INSECURE pseudo-random "
    "data.\n"
    "\n"
    "Options:\n"
    "  -T, --threads=THREADS    Number of threads to use. (default: 2)\n"
    "  -c, --bytes=BYTES        Number of bytes to generate, or '-1' for "
    "unlimited.\n"
    "                           (default: -1)\n"
    "  -h, --help               Show help.\n"
    "  -V, --version            Show version.\n";

static void
exit_err_inner()
{
    exit(1);
}

static void
exit_err()
{
    static once_flag flag = ONCE_FLAG_INIT;
    call_once(&flag, exit_err_inner);
}

static void
get_random_data(size_t size, void* ptr)
{
    FILE* urandom;
    if ((urandom = fopen("/dev/urandom", "r")) == NULL) {
        perror("fastrandom: '/dev/urandom'");
        exit_err();
    }
    if (setvbuf(urandom, NULL, _IONBF, 0) != 0) {
        perror("fastrandom: '/dev/urandom'");
    }
    if (fread(ptr, size, 1, urandom) != 1) {
        perror("fastrandom: '/dev/urandom'");
        exit_err();
    }
    fclose(urandom);
}

static size_t
write_data(size_t size, void* buf)
{
    if (!infinite_stream) {
        unsigned long rem_copy = remaining_bytes;
        if (rem_copy == 0)
            return 0;
        do {
            if (rem_copy < size)
                size = rem_copy;
        } while (atomic_compare_exchange_weak(&remaining_bytes, &rem_copy, rem_copy - size));
    }
    if (fwrite(buf, 1, size, stdout) != size) {
        perror("fastrandom: fwrite");
        exit_err();
    }

    return size;
}

static int
fastrandom_stream(void* arg)
{
    uint64_t state[2] = { 0, 0 };
    uint64_t* buf = calloc(BUFFER_SIZE, sizeof(uint64_t));

    do {
        /* If the initial state is all zeroes, the algorithm will produce
             a stream of zeros, so let's make sure this won't happen. */
        get_random_data(sizeof(state), state);
    } while (state[0] == 0 && state[1] == 0);

    for (;;) {
        for (int i = 0; i < BUFFER_SIZE; i++) {
            uint64_t t = state[0];
            uint64_t const s = state[1];
            state[0] = s;
            t ^= t << 23;
            t ^= t >> 18;
            t ^= s ^ (s >> 5);
            state[1] = t;
            buf[i] = t + s;
        }
        const unsigned bytes = BUFFER_SIZE * sizeof(uint64_t);
        if (write_data(bytes, buf) < bytes) {
            break;
        }
    }
    free(buf);

    return 0;
}

static void
parse_args(int argc, char* argv[])
{
    int c;
    char* endptr;

    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            { "bytes", required_argument, 0, 'c' },
            { "threads", required_argument, 0, 'T' },
            { "help", no_argument, 0, 'h' },
            { "version", no_argument, 0, 'V' },
            { 0, 0, 0, 0 }
        };

        c = getopt_long(argc, argv, "c:T:hV", long_options, &option_index);
        if (c == -1)
            break;

        switch (c) {
            case 'h':
                fprintf(stdout, "%s", help_text);
                exit(EXIT_SUCCESS);
                break;

            case 'V':
                fprintf(stdout, "%s", version);
                exit(EXIT_SUCCESS);
                break;

            case 'c':
                if (strcmp(optarg, "-1") == 0) {
                    infinite_stream = true;
                    remaining_bytes = 0;
                } else {
                    errno = 0;
                    infinite_stream = false;
                    remaining_bytes = strtoul(optarg, &endptr, 10);
                    if (errno != 0 || *endptr != '\0' || remaining_bytes == 0) {
                        fprintf(stderr, "fastrandom: invalid number of bytes: '%s'\n", optarg);
                        exit(EXIT_FAILURE);
                    }
                }
                break;

            case 'T':
                errno = 0;
                number_of_threads = strtoul(optarg, &endptr, 10);
                if (errno != 0 || *endptr != '\0' || number_of_threads == 0 ||
                        number_of_threads > 1024) {
                    fprintf(stderr, "fastrandom: invalid number of threads: '%s'\n", optarg);
                    exit(EXIT_FAILURE);
                }
                break;

            case '?':
                exit(EXIT_FAILURE);
                break;

            default:
                printf("?? getopt returned character code 0%o ??\n", c);
                abort();
        }
    }

    if (optind < argc) {
        fprintf(stderr, "Usage: fastrandom [OPTION]...\n"
                        "Try 'fastrandom --help' for more information.\n");
        exit(EXIT_FAILURE);
    }
}

int
main(int argc, char* argv[])
{
    parse_args(argc, argv);

    if (setvbuf(stdout, NULL, _IONBF, 0) != 0) {
        perror("fastrandom: setvbuf");
    }
    if (number_of_threads > 1) {
        thrd_t* thread = (thrd_t*)calloc(number_of_threads - 1, sizeof(thrd_t));
        for (int i = 0; i < (number_of_threads - 1); i++) {
            if (thrd_create(&thread[i], fastrandom_stream, NULL) != thrd_success) {
                fprintf(stderr, "fastrandom: failed to create thread\n");
                exit_err();
            }
        }
        fastrandom_stream(NULL);
        for (int i = 0; i < (number_of_threads - 1); i++) {
            if (thrd_join(thread[i], NULL) != thrd_success) {
                fprintf(stderr, "fastrandom: failed to join thread\n");
                exit_err();
            }
        }
        free(thread);
    } else {
        fastrandom_stream(NULL);
    }

    return EXIT_SUCCESS;
}
