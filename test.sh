#!/bin/bash
test_failed=0


# Test if output looks random enough
randomness_test() {
    ./fastrandom "$@" | rngtest -c 1000 >/tmp/test_result 2>&1
    successes="$(sed -E -e '/FIPS 140-2 successes/ { s,.*: ,,gp }; d' /tmp/test_result)"
    failures="$(sed -E -e '/FIPS 140-2 failures/ { s,.*: ,,gp }; d' /tmp/test_result)"
    longruns="$(sed -E -e '/FIPS 140-2.* Continuous run/ { s,.*: ,,gp }; d' /tmp/test_result)"
    rm /tmp/test_result
    test "$failures" -le 8 -a "$longruns" -eq 0 && success=1 || success=0
    test "$success" -eq 1 && status=ok || status=failed
    printf "%4d, %2d, %2d: %s  (fastrandom $*)\n" "$successes" "$failures" "$longruns" "$status"
    test $status = failed && test_failed=1
}

for T in 1 2 3 4; do
    # shellcheck disable=SC2034
    for r in 1 2 3 4; do
        randomness_test -T $T
        randomness_test -T $T -c 2500000
    done
done

# Test if output always has the correct size
size_test() {
    EXPECT=$1
    shift
    COUNT="$(./fastrandom "$@" -c "$EXPECT" | wc -c)"
    test "$COUNT" -eq "$EXPECT" && status=ok || status=failed
    printf "%9d: $status  (fastrandom $* -c $EXPECT)\n" "$COUNT"
    test $status = failed && test_failed=1
}

for T in 1 2 3 4; do
    for S in $(seq 1 23 500); do
        SIZE=$(( (S*S*S) ))
        size_test $SIZE -T $T
    done
done

test $test_failed -eq 1 && exit 1 || exit 0
