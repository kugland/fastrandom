% FASTRANDOM(1) | fastrandom
% André Kugland
% January 2022

NAME
====

fastrandom - Quickly generate large amounts of cryptographically *insecure*
pseudo-random data.

SYNOPSIS
========

**fastrandom** \[*OPTIONS*\]

DESCRIPTION
===========

**fastrandom** uses xorshift128+ algorithm to quickly generate large amounts
of cryptographically *insecure* pseudo-random data.

If you need to generate cryptographically secure random data, please use
*/dev/urandom*.

OPTIONS
=======

**\-T** *THREADS*, **\-\-threads**=*THREADS*
: Number of threads to use (default: 2).

**\-c** *BYTES*, **\-\-bytes**=*BYTES*
: Number of bytes to generate, -1 for unlimited (default: -1).

**\-h**, **\-\-help**
: Show help.

**\-V**, **\-\-version**
: Show version.

REPORTING BUGS
==============

Please report any bugs or feature requests to <https://gitlab.com/kugland/fastrandom/-/issues>.

SEE ALSO
========
**urandom**(4).
